import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from "@angular/router";
import {AppComponent} from "./app.component";
import {HandsontableComponent} from "./handsontable/handsontable.component";
import {DhtmlxComponent} from "./dhtmlx/dhtmlx.component";

const appRoutes: Routes = [
  {path: 'handsontable', component: HandsontableComponent},
  {path: 'dhtmlxspreadsheet', component: DhtmlxComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      enableTracing: false,
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  exports: [RouterModule],
})
export class AppRouting {
}
