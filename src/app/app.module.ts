import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HotTableModule} from '@handsontable/angular';
import {MaterialDashboardComponent} from './material-dashboard/material-dashboard.component';
import {MatGridListModule, MatCardModule, MatMenuModule, MatIconModule, MatButtonModule} from '@angular/material';
import {LayoutModule} from '@angular/cdk/layout';
import {HandsontableComponent} from './handsontable/handsontable.component';
import {AppRouting} from "./app.routing";
import { DhtmlxComponent } from './dhtmlx/dhtmlx.component';

@NgModule({
  declarations: [
    AppComponent,
    MaterialDashboardComponent,
    HandsontableComponent,
    DhtmlxComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HotTableModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,

    AppRouting
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
